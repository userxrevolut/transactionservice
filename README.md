# TransactionService

Running the service
through Main class (the first parameter is the port to which server connects, by default 80
org.nk.revolut.services.transaction.Main
java -jar target/transaction-sevice-1.0-SNAPSHOT-jar-with-dependencies.jar

Minimal version supporting one currency, and transaction contains money sent + account information
{
  "sourceAccount": {
    "accountNumber": "1"
  },
  "targetAccount": {
    "accountNumber": "2"
  },
  "transferedAmount": {
    "amount": "10",
    "currency": "USD"
  }
}

Usage (windows)
wget --post-file src/test/resources/requests/samplePostTransaction.json --header="Content-Type: application/json" localhost/transactions

Info Due to nature of this projects some accounts are initialized in the creation scrip (with account numbers 1,2,3 see: dbSchema.sql)

Comments

dto
- setters are mainly for JSON transformation, passing data in constructors seems to require repeating property name in JSONParameter; only Money is VO
- ignored serialization tests for getter/setter cases as this will be by-tested in integration tests
rest
- no formal model (raml?), just java classes rest package + dto
database
- schema was created manually, one of the reasons is I need to insert account information for in memory db (easy way with h2 init script)
- DbSchemaTest would not be normally present, but since schema is created in memory not in normal db using sql developer, I needed to test it
domain vs dto package
- domain contains DB objects (just my current project standard) not exposed by the service
- dto works as objects used for data transfer in rest layer + are inputs/outputs to the service
(in larger project it might be needed to extract separate/proper domain objects)
- in all objects sets those objects are dummy: no behaviour (like account.transfer(targetAccount,money), that is standard spring+hibernate application
service package
- with more classes you could create subpackages validation/exceptions etc
bootstrap
= no tests there as this should be tested by integration test (mainly creation and setting of objects)

- no integration tests due to lack of time

