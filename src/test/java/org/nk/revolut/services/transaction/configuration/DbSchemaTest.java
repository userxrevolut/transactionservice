package org.nk.revolut.services.transaction.configuration;

import org.junit.Test;
import org.nk.revolut.services.transaction.Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class DbSchemaTest {

    private Connection createConnection() throws SQLException {
        ServiceConfigurationLoader loader = new ServiceConfigurationLoader();
        ServiceConfiguration configuration = loader.loadFromClassPath(Main.DEFAULT_SERVICE_CONFIGURATION_PATH);
        return DriverManager.getConnection(configuration.getDatabaseConnection(), "sa", null);
    }

    private void addTransaction(String sourceAccountId, String targetAccountId, boolean sourcePresent, boolean targetPresent, int amount) throws SQLException {
        try (Connection con = createConnection()) {
            if (sourcePresent) {
                assertEquals(1, con.createStatement().executeUpdate(
                        "INSERT INTO ACCOUNT (ACCOUNT_NUMBER,BALANCE) VALUES('" + sourceAccountId + "',1)"));
            }
            if (targetPresent) {
                assertEquals(1, con.createStatement().executeUpdate(
                        "INSERT INTO ACCOUNT (ACCOUNT_NUMBER,BALANCE) VALUES('" + targetAccountId + "',0)"));
            }
            assertEquals(1, con.createStatement().executeUpdate(
                    "INSERT INTO BANK_TRANSACTION (SOURCE_ACCOUNT_NUMBER, TARGET_ACCOUNT_NUMBER,AMOUNT) VALUES('" +
                            sourceAccountId + "','" + targetAccountId + "', " + amount + ")"));
        }
    }

    @Test
    public void referenceIntegrityPositive() throws SQLException {
        addTransaction("8", "9", true, true, 1);
    }

    @Test(expected = SQLException.class)
    public void referenceIntegrityNegativeSourceNotPresent() throws SQLException {
        addTransaction("8", "9", false, true, 1);
    }

    @Test(expected = SQLException.class)
    public void referenceIntegrityNegativeTargetNotPresent() throws SQLException {
        addTransaction("8", "9", true, false, 1);
    }

    @Test(expected = SQLException.class)
    public void checkOnTransactionAmount() throws SQLException {
        addTransaction("8", "9", true, true, 0);
    }

    @Test(expected = SQLException.class)
    public void checkOnAccountBalance() throws SQLException {
        try (Connection con = createConnection()) {
            con.createStatement().executeUpdate("INSERT INTO ACCOUNT (ACCOUNT_NUMBER,BALANCE) VALUES('9',-1)");
        }
    }
}
