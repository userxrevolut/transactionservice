package org.nk.revolut.services.transaction.repositories;

import org.junit.Test;
import org.nk.revolut.services.transaction.domain.Account;
import org.nk.revolut.services.transaction.domain.Transaction;

import javax.persistence.EntityTransaction;
import java.math.BigDecimal;

import static org.junit.Assert.assertNotNull;

public class JPATransactionRepositoryTest {
    @Test
    public void saveNew() throws Exception {
        ThreadLocalEntityManagerProvider provider = DBHelper.getEntityManagerProvider("transactionTest.saveNew");
        JPAAccountRepository accountRepository = new JPAAccountRepository(provider);
        JPATransactionRepository repository = new JPATransactionRepository(provider);

        EntityTransaction entityTransaction = provider.get().getTransaction();
        entityTransaction.begin();//nneded for hibernate

        Account source = accountRepository.findByAccoundNumber(DBHelper.ACCOUNT_ID_USD10);
        Account target = accountRepository.findByAccoundNumber(DBHelper.ACCOUNT_ID_USD0);

        Transaction transaction = new Transaction();
        transaction.setSourceAccount(source);
        transaction.setTargetAccount(target);
        transaction.setAmount(BigDecimal.ONE);

        transaction = repository.saveNew(transaction);

        assertNotNull(transaction.getId());

        entityTransaction.rollback();

        provider.clear();
    }

    @Test(expected = IllegalStateException.class)
    public void dontSaveNotNewTransaction() throws Exception {
        ThreadLocalEntityManagerProvider provider = DBHelper.getEntityManagerProvider("transactionTest.dontSaveNotNewTransaction");
        JPATransactionRepository repository = new JPATransactionRepository(provider);

        Transaction transaction = new Transaction();
        transaction.setId("1");

        try {
            repository.saveNew(transaction);
        } finally {
            provider.clear();
        }
    }

    @Test(expected = Exception.class)
    public void dontSaveEmptyTransaction() throws Exception {
        ThreadLocalEntityManagerProvider provider = DBHelper.getEntityManagerProvider("transactionTest.dontSaveEmptyTransaction");
        JPATransactionRepository repository = new JPATransactionRepository(provider);

        Transaction transaction = new Transaction();

        try {
            repository.saveNew(transaction);
        } finally {
            provider.clear();
        }
    }
}