package org.nk.revolut.services.transaction.services.validation;

import org.junit.Test;
import org.nk.revolut.services.transaction.dto.Money;
import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

import java.math.BigDecimal;

public class CurrencyValidatorTest {

    private CurrencyValidator validator = new CurrencyValidator("USD");

    private TransactionRequest createTransactionRequest(String currency) {
        TransactionRequest request = new TransactionRequest();
        request.setTransferedAmount(new Money(BigDecimal.ZERO, currency));
        return request;
    }

    private void validate(String currency) throws ValidationException {
        validator.validate(createTransactionRequest(currency));
    }

    @Test
    public void validateSuccess() throws Exception {
        validate("USD");
    }

    @Test(expected = ValidationException.class)
    public void validateUnsupportedurrency() throws Exception {
        validate("PLN");
    }

    @Test(expected = ValidationException.class)
    public void validateCurrnecyMissing() throws Exception {
        validate(null);
    }

    @Test(expected = ValidationException.class)
    public void validateMoneyMissing() throws Exception {
        validator.validate(new TransactionRequest());
    }

}