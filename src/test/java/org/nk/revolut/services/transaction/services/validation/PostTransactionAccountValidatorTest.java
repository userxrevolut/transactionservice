package org.nk.revolut.services.transaction.services.validation;

import org.junit.Test;
import org.nk.revolut.services.transaction.domain.Account;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

import java.math.BigDecimal;

public class PostTransactionAccountValidatorTest {

    private PostTransactionAccountValidator validator = new PostTransactionAccountValidator(new BigDecimal("100"));

    @Test
    public void validateMaximumAmountNotExceeded() throws Exception {
        Account account = new Account();
        account.setBalance(BigDecimal.ZERO);
        account.setBalance(new BigDecimal("100"));
        validator.validate(account);
    }

    @Test(expected = ValidationException.class)
    public void validateMaximumAmountExceeded() throws Exception {
        Account account = new Account();
        account.setBalance(new BigDecimal("101"));
        validator.validate(account);
    }

}