package org.nk.revolut.services.transaction.services.validation;

import org.junit.Test;
import org.nk.revolut.services.transaction.dto.Money;
import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

import java.math.BigDecimal;

public class AmountValidatorTest {

    private AmountValidator validator = new AmountValidator(2);

    private TransactionRequest createTransactionRequest(BigDecimal amount) {
        TransactionRequest request = new TransactionRequest();
        request.setTransferedAmount(new Money(amount, "USD"));
        return request;
    }

    private void validate(BigDecimal amount) throws ValidationException {
        validator.validate(createTransactionRequest(amount));
    }

    @Test
    public void validateSuccess() throws Exception {
        validate(new BigDecimal("0.01"));
        validate(new BigDecimal("1"));
        validate(new BigDecimal("1.00"));
        validate(new BigDecimal("10000"));
        validate(BigDecimal.valueOf(1, -3));
    }

    @Test(expected = ValidationException.class)
    public void validateScaleSuperseeded() throws Exception {
        validate(new BigDecimal("0.001"));
    }

    @Test(expected = ValidationException.class)
    public void validateAmountMissing() throws Exception {
        validate(null);
    }

    @Test(expected = ValidationException.class)
    public void validateMoneyMissing() throws Exception {
        validator.validate(new TransactionRequest());
    }
}