package org.nk.revolut.services.transaction.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class MoneyTest {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void seriallizedMoneyHasAllTheProperties() throws Exception {
        Money original = new Money(new BigDecimal(1), "USD");
        String serialized = objectMapper.writeValueAsString(original);
        Money deserialized = objectMapper.readValue(serialized, Money.class);
        assertEquals(original.getAmount(), deserialized.getAmount());
        assertEquals(original.getCurrency(), deserialized.getCurrency());
    }
}