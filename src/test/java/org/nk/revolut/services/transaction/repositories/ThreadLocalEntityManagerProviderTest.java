package org.nk.revolut.services.transaction.repositories;

import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static org.mockito.Mockito.*;

public class ThreadLocalEntityManagerProviderTest {

    @Test
    public void getCreatesJustOneEntityManager() throws Exception {
        EntityManagerFactory emf = mock(EntityManagerFactory.class);
        when(emf.createEntityManager()).thenReturn(mock(EntityManager.class));
        ThreadLocalEntityManagerProvider provider = new ThreadLocalEntityManagerProvider(emf);

        provider.get();
        provider.get();

        verify(emf, Mockito.times(1)).createEntityManager();
    }

    @Test
    public void verifyObjectIsCleared() throws Exception {
        EntityManagerFactory emf = mock(EntityManagerFactory.class);
        when(emf.createEntityManager()).thenReturn(mock(EntityManager.class));
        ThreadLocalEntityManagerProvider provider = new ThreadLocalEntityManagerProvider(emf);

        provider.get();
        provider.clear();
        provider.get();

        verify(emf, Mockito.times(2)).createEntityManager();
    }

}