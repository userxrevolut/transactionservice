package org.nk.revolut.services.transaction.rest;

import org.junit.Test;
import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.dto.TransactionResult;
import org.nk.revolut.services.transaction.services.BankTransactionService;
import org.nk.revolut.services.transaction.services.ErrorCodes;
import org.nk.revolut.services.transaction.util.DBTransactionUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TransactionServiceRestTest {

    @Test
    public void serviceIsRunWithinTransaction() throws Exception {
        BankTransactionService service = mock(BankTransactionService.class);
        DBTransactionUtil dbTransactionUtil = mock(DBTransactionUtil.class);
        TransactionServiceRest rest = new TransactionServiceRest(service, dbTransactionUtil);

        rest.postTransaction(new TransactionRequest());

        verify(dbTransactionUtil, times(1)).<TransactionResult>runInTransaction(
                any(), any());
    }

    @Test
    public void exceptionInServiceIsReturnAsErrorCode() throws Exception {
        BankTransactionService service = mock(BankTransactionService.class);
        DBTransactionUtil dbTransactionUtil = mock(DBTransactionUtil.class);
        TransactionServiceRest rest = new TransactionServiceRest(service, dbTransactionUtil);
        when(dbTransactionUtil.runInTransaction(any(), any())).thenThrow(new RuntimeException());

        TransactionResult result = rest.postTransaction(new TransactionRequest());

        assertFalse(result.isSuccessful());
        assertEquals(ErrorCodes.OtherError.name(), result.getErrorCode());
    }

}