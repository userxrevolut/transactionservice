package org.nk.revolut.services.transaction.util;

import org.junit.Before;
import org.junit.Test;
import org.nk.revolut.services.transaction.repositories.ThreadLocalEntityManagerProvider;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import static org.mockito.Mockito.*;

public class DBTransactionUtilTest {

    private DBTransactionUtil util;
    private EntityTransaction transaction;

    @Before
    public void setup() {
        EntityManager entityManager = mock(EntityManager.class);
        transaction = mock(EntityTransaction.class);
        when(entityManager.getTransaction()).thenReturn(transaction);
        ThreadLocalEntityManagerProvider provider = mock(ThreadLocalEntityManagerProvider.class);
        when(provider.get()).thenReturn(entityManager);

        util = new DBTransactionUtil(provider);
    }

    @Test
    public void transactionIsCommitedAfterSucces() throws Exception {

        util.runInTransaction(() -> "", ret -> true);

        verify(transaction, times(1)).begin();
        verify(transaction, times(1)).commit();
        verify(transaction, times(0)).rollback();
    }

    @Test
    public void transactionIsRolledbackAfterFailure() throws Exception {
        util.runInTransaction(() -> "", ret -> false);

        verify(transaction, times(1)).begin();
        verify(transaction, times(0)).commit();
        verify(transaction, times(1)).rollback();
    }

    @Test(expected = Exception.class)
    public void transactionIsRolledbackAfterException() throws Exception {

        try {
            util.runInTransaction(() -> {
                throw new Exception();
            }, ret -> true);
        } finally {
            verify(transaction, times(1)).begin();
            verify(transaction, times(0)).commit();
            verify(transaction, times(1)).rollback();
        }
    }
}