package org.nk.revolut.services.transaction.services.validation;

import org.junit.Test;
import org.nk.revolut.services.transaction.dto.AccountInformation;
import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

public class AccountInformationValidatorTest {

    private AccountInformationValidator validator = new AccountInformationValidator();

    private TransactionRequest createRequest(String sourceAccountNumber, String targetAccountNumber) {
        TransactionRequest request = new TransactionRequest();

        AccountInformation sourceAccount = new AccountInformation();
        sourceAccount.setAccountNumber(sourceAccountNumber);
        request.setSourceAccount(sourceAccount);

        AccountInformation targetAccount = new AccountInformation();
        targetAccount.setAccountNumber(targetAccountNumber);
        request.setTargetAccount(targetAccount);

        return request;
    }

    @Test
    public void validateSuccessAllAcountInformationPresent() throws Exception {
        validator.validate(createRequest("1", "2"));
    }

    @Test(expected = ValidationException.class)
    public void validateTargetAccountNumberNotPresent() throws Exception {
        validator.validate(createRequest("1", null));
    }

    @Test(expected = ValidationException.class)
    public void validatSourceAccountNumberNotPresent() throws Exception {
        validator.validate(createRequest(null, "2"));
    }

    @Test(expected = ValidationException.class)
    public void validateTargetAccountNotPresent() throws Exception {
        TransactionRequest request = createRequest("1", "2");
        request.setTargetAccount(null);
        validator.validate(request);
    }

    @Test(expected = ValidationException.class)
    public void validateSourceAccountNotPresent() throws Exception {
        TransactionRequest request = createRequest("1", "2");
        request.setSourceAccount(null);
        validator.validate(request);
    }

}