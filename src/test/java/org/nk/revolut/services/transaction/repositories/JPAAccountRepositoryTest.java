package org.nk.revolut.services.transaction.repositories;

import org.junit.Test;
import org.nk.revolut.services.transaction.domain.Account;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JPAAccountRepositoryTest {

    @Test
    public void findByAccoundNumber() throws Exception {
        ThreadLocalEntityManagerProvider provider = DBHelper.getEntityManagerProvider("accountTest.findByAccoundNumber");
        JPAAccountRepository repository = new JPAAccountRepository(provider);

        Account account = repository.findByAccoundNumber(DBHelper.ACCOUNT_ID_USD10);
        assertNotNull(account);
        assertEquals(10, account.getBalance().intValue());
        assertEquals(DBHelper.ACCOUNT_ID_USD10, account.getAccountNumber());

        provider.clear();
    }

    @Test
    public void update() throws Exception {
        ThreadLocalEntityManagerProvider provider = DBHelper.getEntityManagerProvider("accountTest.update");
        JPAAccountRepository repository = new JPAAccountRepository(provider);

        Account account = repository.findByAccoundNumber(DBHelper.ACCOUNT_ID_USD5);
        account.setBalance(new BigDecimal("1"));
        repository.update(account);
        assertEquals(1, account.getBalance().intValue());
        Account reloaded = repository.findByAccoundNumber(DBHelper.ACCOUNT_ID_USD5);
        assertEquals(1, reloaded.getBalance().intValue());

        provider.clear();
    }

}