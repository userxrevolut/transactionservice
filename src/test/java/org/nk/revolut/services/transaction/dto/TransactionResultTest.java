package org.nk.revolut.services.transaction.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.*;

public class TransactionResultTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void createSuccessfulResultHasOnlySuccessInformation() throws Exception {
        TransactionResult result = TransactionResult.createSuccessfulResult("1");
        assertEquals("1", result.getTransactionId());
        assertNull(result.getErrorCode());
        assertTrue(result.isSuccessful());
        String seriallized = objectMapper.writeValueAsString(result);
        assertTrue(seriallized.contains("transactionId"));
        assertFalse(seriallized.contains("errorCode"));
    }

    @Test
    public void createFailedResultHasOnlyFailedInformation() throws Exception {
        TransactionResult result = TransactionResult.createFailedResult("1");
        assertEquals("1", result.getErrorCode());
        assertNull(result.getTransactionId());
        assertFalse(result.isSuccessful());
        String seriallized = objectMapper.writeValueAsString(result);
        assertFalse(seriallized.contains("transactionId"));
        assertTrue(seriallized.contains("errorCode"));
    }

}