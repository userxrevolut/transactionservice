package org.nk.revolut.services.transaction.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.nk.revolut.services.transaction.configuration.ValidationConfiguration;
import org.nk.revolut.services.transaction.domain.Account;
import org.nk.revolut.services.transaction.domain.Transaction;
import org.nk.revolut.services.transaction.dto.AccountInformation;
import org.nk.revolut.services.transaction.dto.Money;
import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.dto.TransactionResult;
import org.nk.revolut.services.transaction.repositories.AccountRepository;
import org.nk.revolut.services.transaction.repositories.TransactionRepository;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

public class SingleCurrencyTransactionServiceTest {

    private static final String SAVED_TRANSACTION_ID = "777";
    private static final String REAL_ACCOUNT_60 = "1";
    private static final String REAL_ACCOUNT_70 = "2";
    private static final String NOT_EXISTANT_ACCOUNT = "3";

    private AccountRepository accountRepository;
    private TransactionRepository transactionRepository;
    private ValidationConfiguration validationConfiguration;

    private SingleCurrencyTransactionService service;

    private ArgumentCaptor<Account> accountCapture;
    private BigDecimal transactionValue;

    private AccountRepository createAccountRepository() {
        AccountRepository repository = mock(AccountRepository.class);

        Account sourceAccount = new Account();
        sourceAccount.setBalance(BigDecimal.valueOf(60));
        Mockito.when(repository.findByAccoundNumber(REAL_ACCOUNT_60)).thenReturn(sourceAccount);

        Account targetAccount = new Account();
        targetAccount.setBalance(BigDecimal.valueOf(70));
        Mockito.when(repository.findByAccoundNumber(REAL_ACCOUNT_70)).thenReturn(targetAccount);

        accountCapture = ArgumentCaptor.forClass(Account.class);
        Mockito.when(repository.update(accountCapture.capture())).then(anwser -> anwser.getArgument(0));
        transactionValue = null;

        return repository;
    }

    private TransactionRepository createTransactionRepository() {
        TransactionRepository repository = mock(TransactionRepository.class);
        Mockito.when(repository.saveNew(any())).then(anwser -> {
            Transaction transaction = anwser.getArgument(0);
            transaction.setId(SAVED_TRANSACTION_ID);
            transactionValue = transaction.getAmount();
            return transaction;
        });
        return repository;
    }

    private ValidationConfiguration createValidationConfiguration() {
        ValidationConfiguration validationConfiguration = new ValidationConfiguration();
        validationConfiguration.setMaximumAccountBalace(new BigDecimal(100));
        validationConfiguration.setSupportedCurrency("USD");
        validationConfiguration.setMaximumScale(2);
        return validationConfiguration;
    }

    @Before
    public void setup() {
        validationConfiguration = createValidationConfiguration();
        accountRepository = createAccountRepository();
        transactionRepository = createTransactionRepository();
        service = new SingleCurrencyTransactionService(accountRepository, transactionRepository, validationConfiguration);
    }

    private TransactionRequest createTransactionRequest(double transferAmount, String currency) {
        return createTransactionRequest(transferAmount, currency, REAL_ACCOUNT_60, REAL_ACCOUNT_70);
    }

    private TransactionRequest createTransactionRequest(double transferAmount, String currency, String sourceAccountNumber, String targetAccountNumber) {
        TransactionRequest request = new TransactionRequest();

        AccountInformation sourceAccountInformation = new AccountInformation();
        sourceAccountInformation.setAccountNumber(sourceAccountNumber);
        request.setSourceAccount(sourceAccountInformation);

        AccountInformation targetAccountInformation = new AccountInformation();
        targetAccountInformation.setAccountNumber(targetAccountNumber);
        request.setTargetAccount(targetAccountInformation);

        request.setTransferedAmount(new Money(BigDecimal.valueOf(transferAmount), currency));

        return request;
    }

    private void validateNoTransactionsAndAccountsSaved() {
        assertEquals(0, accountCapture.getAllValues().size());
        assertNull(transactionValue);
    }

    private void validateTransactionsAndAccountsSaved(int transactionAmount, int sourceAccountBalance, int targetAccountBalance) {
        assertEquals(2, accountCapture.getAllValues().size());
        assertEquals(0, BigDecimal.valueOf(sourceAccountBalance).compareTo(accountCapture.getAllValues().get(0).getBalance()));
        assertEquals(0, BigDecimal.valueOf(targetAccountBalance).compareTo(accountCapture.getAllValues().get(1).getBalance()));
        assertEquals(0, BigDecimal.valueOf(transactionAmount).compareTo(this.transactionValue));
    }

    @Test
    public void happyPath() throws Exception {
        TransactionResult result = service.performTransaction(createTransactionRequest(1, "USD"));
        assertTrue(result.isSuccessful());
        assertEquals(SAVED_TRANSACTION_ID, result.getTransactionId());
        validateTransactionsAndAccountsSaved(1, 59, 71);
    }

    @Test
    public void selfTransferCheck() throws Exception {
        TransactionResult result = service.performTransaction(createTransactionRequest(1, "USD", REAL_ACCOUNT_60, REAL_ACCOUNT_60));
        assertTrue(result.isSuccessful());
        assertEquals(SAVED_TRANSACTION_ID, result.getTransactionId());
        validateTransactionsAndAccountsSaved(1, 60, 60);
    }

    private void validateErrorOccured(TransactionResult result, ErrorCodes code) {
        assertFalse(result.isSuccessful());
        assertEquals(code.name(), result.getErrorCode());
        validateNoTransactionsAndAccountsSaved();
    }

    @Test
    public void currencyValidatorIsExecuted() throws Exception {
        TransactionResult result = service.performTransaction(createTransactionRequest(1, "PLN"));
        validateErrorOccured(result, ErrorCodes.UnsupportedCurrency);
    }

    @Test
    public void amountValidatorIsExecuted() throws Exception {
        TransactionResult result = service.performTransaction(createTransactionRequest(0.001, "USD"));
        validateErrorOccured(result, ErrorCodes.WrongAmount);
    }

    @Test
    public void accountInformationValidatorIsExecuted() throws Exception {
        TransactionRequest request = createTransactionRequest(0.001, "PLN");
        request.setSourceAccount(null);
        TransactionResult result = service.performTransaction(request);
        validateErrorOccured(result, ErrorCodes.AccountNotFould);
    }

    @Test
    public void postTransactionAccountValidatorIsExecuted() throws Exception {
        TransactionResult result = service.performTransaction(createTransactionRequest(31, "USD"));
        validateErrorOccured(result, ErrorCodes.OtherError);
    }

    @Test
    public void sufficientAmountOfFundsCheckIsPerformed() throws Exception {
        TransactionResult result = service.performTransaction(createTransactionRequest(90, "USD"));
        validateErrorOccured(result, ErrorCodes.InsufficientFunds);
    }

    @Test
    public void sourceAccountExistentCheckIsPeformed() throws Exception {
        TransactionResult result = service.performTransaction(createTransactionRequest(1, "USD", NOT_EXISTANT_ACCOUNT, REAL_ACCOUNT_70));
        validateErrorOccured(result, ErrorCodes.AccountNotFould);
    }

    @Test
    public void targetAccountExistentCheckIsPeformed() throws Exception {
        TransactionResult result = service.performTransaction(createTransactionRequest(1, "USD", REAL_ACCOUNT_70, NOT_EXISTANT_ACCOUNT));
        validateErrorOccured(result, ErrorCodes.AccountNotFould);
    }

}