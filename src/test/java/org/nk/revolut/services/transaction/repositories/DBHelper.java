package org.nk.revolut.services.transaction.repositories;

import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class DBHelper {

    public static final String ACCOUNT_ID_USD5 = "7";
    public static final String ACCOUNT_ID_USD0 = "8";
    public static final String ACCOUNT_ID_USD10 = "9";

    public static ThreadLocalEntityManagerProvider threadLocalEntityManagerProvider;

    public static ThreadLocalEntityManagerProvider getEntityManagerProvider(String dbName) {
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.connection.url", "jdbc:h2:mem:" + dbName + ";INIT=RUNSCRIPT FROM 'classpath:/configuration/dbschema.sql'\\; RUNSCRIPT FROM 'classpath:/testdata/accountdata.sql'");
        return new ThreadLocalEntityManagerProvider(
                Persistence.createEntityManagerFactory("bankTransactionPersistanceUnit", properties));
    }
}
