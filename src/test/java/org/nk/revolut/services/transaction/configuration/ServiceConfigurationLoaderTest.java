package org.nk.revolut.services.transaction.configuration;

import org.junit.Test;
import org.nk.revolut.services.transaction.Main;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class ServiceConfigurationLoaderTest {
    @Test
    public void loadFromClassPath() throws Exception {
        ServiceConfigurationLoader loader = new ServiceConfigurationLoader();
        ServiceConfiguration configuration = loader.loadFromClassPath(Main.DEFAULT_SERVICE_CONFIGURATION_PATH);

        assertEquals(configuration.getDefaultServerPort(), 80);
    }

    @Test
    public void loadValidationFromClassPath() throws Exception {
        ServiceConfigurationLoader loader = new ServiceConfigurationLoader();
        ValidationConfiguration configuration = loader.loadValidationConfigurationFromClassPath(Main.DEFAULT_SERVICE_VALIDATION_CONFIGURATION_PATH);

        assertEquals(configuration.getSupportedCurrency(), "USD");
        assertEquals(configuration.getMaximumScale(), 2);
        assertEquals(configuration.getMaximumAccountBalace(), new BigDecimal("1000"));
    }

}