package org.nk.revolut.services.transaction.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "BANK_TRANSACTION")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private String id;
    @ManyToOne
    @JoinColumn(name = "SOURCE_ACCOUNT_NUMBER")
    private Account sourceAccount;
    @ManyToOne
    @JoinColumn(name = "TARGET_ACCOUNT_NUMBER")
    private Account targetAccount;
    @Column(name = "AMOUNT")
    private BigDecimal amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public Account getTargetAccount() {
        return targetAccount;
    }

    public void setTargetAccount(Account targetAccount) {
        this.targetAccount = targetAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
