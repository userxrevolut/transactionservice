package org.nk.revolut.services.transaction.util;

import org.nk.revolut.services.transaction.repositories.ThreadLocalEntityManagerProvider;
import org.nk.revolut.services.transaction.rest.TransactionServiceRest;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class DBTransactionUtil {

    private static Logger logger = Logger.getLogger(TransactionServiceRest.class.getCanonicalName());

    private final ThreadLocalEntityManagerProvider entityManagerProvider;

    public DBTransactionUtil(ThreadLocalEntityManagerProvider entityManagerProvider) {
        this.entityManagerProvider = entityManagerProvider;
    }

    public <T> T runInTransaction(Callable<T> action, Predicate<T> wasActionSuccessful) throws Exception {
        EntityManager entityManager = entityManagerProvider.get();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            T result = action.call();
            if (wasActionSuccessful.test(result)) {
                transaction.commit();
            } else {
                transaction.rollback();
            }
            return result;
        } catch (Exception t) {
            transaction.rollback();
            throw t;
        } finally {
            try {
                entityManagerProvider.get().close();
            } catch (Exception ex) {
                logger.severe(ex.getMessage());
            }
            entityManagerProvider.clear();
        }
    }

}
