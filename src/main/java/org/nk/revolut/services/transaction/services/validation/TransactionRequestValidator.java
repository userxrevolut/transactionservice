package org.nk.revolut.services.transaction.services.validation;

import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

public interface TransactionRequestValidator {
    void validate(TransactionRequest request) throws ValidationException;
}
