package org.nk.revolut.services.transaction.services.validation;

import org.nk.revolut.services.transaction.domain.Account;
import org.nk.revolut.services.transaction.services.ErrorCodes;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

import java.math.BigDecimal;

public class PostTransactionAccountValidator {
    private final BigDecimal maximumAccountBalance;

    public PostTransactionAccountValidator(BigDecimal maximumAccountBalance) {
        this.maximumAccountBalance = maximumAccountBalance;
    }

    public void validate(Account account) throws ValidationException {
        BigDecimal balance = account.getBalance();
        if (account.getBalance().compareTo(maximumAccountBalance) > 0) {
            throw new ValidationException(ErrorCodes.OtherError, "balance", balance);
        }
        //ErrorCodes.InsufficientFunds checked before performing transaction
    }
}
