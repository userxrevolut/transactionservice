package org.nk.revolut.services.transaction.services;

import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.dto.TransactionResult;

public interface BankTransactionService {

    TransactionResult performTransaction(TransactionRequest request);

}
