package org.nk.revolut.services.transaction.repositories;

import org.nk.revolut.services.transaction.domain.Account;

public interface AccountRepository {
    public Account findByAccoundNumber(String accountNumber);

    public Account update(Account account);
}
