package org.nk.revolut.services.transaction.repositories;

import org.nk.revolut.services.transaction.domain.Account;

import javax.persistence.EntityManager;
import java.util.function.Supplier;

public class JPAAccountRepository implements AccountRepository {

    private final Supplier<EntityManager> entityManager;

    public JPAAccountRepository(Supplier<EntityManager> entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Account findByAccoundNumber(String accountNumber) {
        return entityManager.get().find(Account.class, accountNumber);
    }

    @Override
    public Account update(Account account) {
        return entityManager.get().merge(account);
    }
}
