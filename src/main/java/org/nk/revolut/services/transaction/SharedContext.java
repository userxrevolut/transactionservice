package org.nk.revolut.services.transaction;

import org.nk.revolut.services.transaction.bootstrap.Repositories;
import org.nk.revolut.services.transaction.bootstrap.Services;
import org.nk.revolut.services.transaction.services.BankTransactionService;
import org.nk.revolut.services.transaction.util.DBTransactionUtil;

public class SharedContext {

    public static SharedContext instance;

    private final BankTransactionService transactionService;
    private final DBTransactionUtil dbTransactionUtil;

    public SharedContext(BankTransactionService transactionService, DBTransactionUtil dbTransactionUtil) {
        this.transactionService = transactionService;
        this.dbTransactionUtil = dbTransactionUtil;
    }

    public static SharedContext getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Shared context was not yet initialized");
        }
        return instance;
    }

    static void initialize(Repositories repositories, Services services) {
        if (instance != null) {
            throw new IllegalStateException("Shared context was already initialized");
        }
        instance = new SharedContext(services.getTransactionService(), repositories.getTransactionUtil());
    }

    public BankTransactionService getTransactionService() {
        return transactionService;
    }

    public DBTransactionUtil getDbTransactionUtil() {
        return dbTransactionUtil;
    }
}
