package org.nk.revolut.services.transaction.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class TransactionResult {
    private boolean successful;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String transactionId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorCode;

    public TransactionResult(boolean successful, String transactionId, String errorCode) {
        this.successful = successful;
        this.transactionId = transactionId;
        this.errorCode = errorCode;
    }

    public static TransactionResult createSuccessfulResult(String transactionId) {
        return new TransactionResult(true, transactionId, null);
    }

    public static TransactionResult createFailedResult(String errorCode) {
        return new TransactionResult(false, null, errorCode);
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public boolean isSuccessful() {
        return successful;
    }
}
