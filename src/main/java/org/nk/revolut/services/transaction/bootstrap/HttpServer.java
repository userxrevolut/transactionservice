package org.nk.revolut.services.transaction.bootstrap;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.nk.revolut.services.transaction.rest.TransactionServiceRest;

import java.util.Collections;

public class HttpServer {

    private Server server;

    public HttpServer(int port) throws Exception {

        ServletHolder jerseyServlet = new ServletHolder(new ServletContainer(createResrouceConfig()));

        server = new Server(port);
        ServletContextHandler context = new ServletContextHandler(server, "/");
        context.addServlet(jerseyServlet, "/*");
    }

    public void start() throws Exception {
        server.start();
        server.join();
    }

    private ResourceConfig createResrouceConfig() {
        ResourceConfig config = new ResourceConfig();
        config.addProperties(Collections.singletonMap("jersey.config.jsonFeature", JacksonFeature.class.getSimpleName()));
        config.register(JacksonFeature.class);
        config.register(TransactionServiceRest.class);
        return config;
    }
}
