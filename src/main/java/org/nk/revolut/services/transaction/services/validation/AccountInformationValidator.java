package org.nk.revolut.services.transaction.services.validation;

import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.services.ErrorCodes;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

public class AccountInformationValidator implements TransactionRequestValidator {
    @Override
    public void validate(TransactionRequest request) throws ValidationException {
        if (request.getSourceAccount() == null || request.getSourceAccount().getAccountNumber() == null) {
            throw new ValidationException(ErrorCodes.AccountNotFould, "sourceAccount", null);
        }
        if (request.getTargetAccount() == null || request.getTargetAccount().getAccountNumber() == null) {
            throw new ValidationException(ErrorCodes.AccountNotFould, "targetAccount", null);
        }
    }
}
