package org.nk.revolut.services.transaction.services.validation;

import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.services.ErrorCodes;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

import java.math.BigDecimal;

public class AmountValidator implements TransactionRequestValidator {
    private final int maximumScale;

    public AmountValidator(int maximumScale) {
        this.maximumScale = maximumScale;
    }

    @Override
    public void validate(TransactionRequest request) throws ValidationException {
        if (request.getTransferedAmount() == null) {
            throw new ValidationException(ErrorCodes.WrongAmount, "amount", null);
        }
        BigDecimal amount = request.getTransferedAmount().getAmount();
        if (amount == null) {
            throw new ValidationException(ErrorCodes.WrongAmount, "amount", amount);
        }
        if (amount.scale() > maximumScale) {
            throw new ValidationException(ErrorCodes.WrongAmount, "amount", amount);
        }
    }
}
