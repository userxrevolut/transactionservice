package org.nk.revolut.services.transaction.rest;

import org.nk.revolut.services.transaction.SharedContext;
import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.dto.TransactionResult;
import org.nk.revolut.services.transaction.services.BankTransactionService;
import org.nk.revolut.services.transaction.services.ErrorCodes;
import org.nk.revolut.services.transaction.util.DBTransactionUtil;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.logging.Logger;

@Path("/transactions")
public class TransactionServiceRest {

    private static Logger logger = Logger.getLogger(TransactionServiceRest.class.getCanonicalName());

    private final BankTransactionService service;
    private final DBTransactionUtil dbTransactionUtil;

    public TransactionServiceRest() {
        this(SharedContext.getInstance().getTransactionService(), SharedContext.getInstance().getDbTransactionUtil());
    }

    public TransactionServiceRest(BankTransactionService service, DBTransactionUtil dbTransactionUtil) {
        this.service = service;
        this.dbTransactionUtil = dbTransactionUtil;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public TransactionResult postTransaction(TransactionRequest request) {
        try {
            return dbTransactionUtil.runInTransaction(
                    () -> service.performTransaction(request),
                    TransactionResult::isSuccessful
            );
        } catch (Exception ex) {
            logger.severe("Performing transaction failed " + ex.getMessage());
            return TransactionResult.createFailedResult(ErrorCodes.OtherError.name());
        }
    }
}
