package org.nk.revolut.services.transaction.dto;

public class TransactionRequest {
    private AccountInformation sourceAccount;
    private AccountInformation targetAccount;
    private Money transferedAmount;

    public AccountInformation getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(AccountInformation sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public AccountInformation getTargetAccount() {
        return targetAccount;
    }

    public void setTargetAccount(AccountInformation targetAccount) {
        this.targetAccount = targetAccount;
    }

    public Money getTransferedAmount() {
        return transferedAmount;
    }

    public void setTransferedAmount(Money transferedAmount) {
        this.transferedAmount = transferedAmount;
    }
}
