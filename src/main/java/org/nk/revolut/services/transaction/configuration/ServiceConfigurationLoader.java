package org.nk.revolut.services.transaction.configuration;

import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.io.Reader;

public class ServiceConfigurationLoader {
    private Gson gson = new Gson();

    public ServiceConfiguration loadFromClassPath(String location) {
        Reader configurationReader = new InputStreamReader(
                ServiceConfigurationLoader.class.getResourceAsStream(location));
        return gson.fromJson(configurationReader, ServiceConfiguration.class);
    }

    public ValidationConfiguration loadValidationConfigurationFromClassPath(String location) {
        Reader configurationReader = new InputStreamReader(
                ServiceConfigurationLoader.class.getResourceAsStream(location));
        return gson.fromJson(configurationReader, ValidationConfiguration.class);
    }
}
