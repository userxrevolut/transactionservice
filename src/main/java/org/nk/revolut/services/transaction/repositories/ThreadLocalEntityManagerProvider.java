package org.nk.revolut.services.transaction.repositories;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.function.Supplier;

public class ThreadLocalEntityManagerProvider implements Supplier<EntityManager> {

    private EntityManagerFactory entityManagerFactory;

    private ThreadLocal<EntityManager> entityManager = new ThreadLocal<>();

    public ThreadLocalEntityManagerProvider(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public EntityManager get() {
        if (entityManager.get() == null) {
            entityManager.set(entityManagerFactory.createEntityManager());
        }
        return entityManager.get();
    }

    public void clear() {
        if (entityManager.get() != null) {
            entityManager.remove();
        }
    }
}
