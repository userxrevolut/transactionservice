package org.nk.revolut.services.transaction.services.validation;

import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.services.ErrorCodes;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;

public class CurrencyValidator implements TransactionRequestValidator {

    private final String supportedCurrency;

    public CurrencyValidator(String supportedCurrency) {
        this.supportedCurrency = supportedCurrency;
    }

    @Override
    public void validate(TransactionRequest request) throws ValidationException {
        if (request.getTransferedAmount() == null) {
            throw new ValidationException(ErrorCodes.UnsupportedCurrency, "currency", null);
        }
        String usedCurrency = request.getTransferedAmount().getCurrency();
        if (!supportedCurrency.equals(usedCurrency)) {
            throw new ValidationException(ErrorCodes.UnsupportedCurrency, "currency", usedCurrency);
        }
    }
}
