package org.nk.revolut.services.transaction.configuration;

public class ServiceConfiguration {
    private int defaultServerPort;
    private String databaseConnection;

    public int getDefaultServerPort() {
        return defaultServerPort;
    }

    public void setDefaultServerPort(int defaultServerPort) {
        this.defaultServerPort = defaultServerPort;
    }

    public String getDatabaseConnection() {
        return databaseConnection;
    }

    public void setDatabaseConnection(String databaseConnection) {
        this.databaseConnection = databaseConnection;
    }
}
