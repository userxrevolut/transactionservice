package org.nk.revolut.services.transaction.bootstrap;

import org.nk.revolut.services.transaction.configuration.ValidationConfiguration;
import org.nk.revolut.services.transaction.services.BankTransactionService;
import org.nk.revolut.services.transaction.services.SingleCurrencyTransactionService;

public class Services {
    private final BankTransactionService transactionService;

    public Services(Repositories repositories, ValidationConfiguration validationConfiguration) {
        transactionService = new SingleCurrencyTransactionService(
                repositories.getAccountRepository(),
                repositories.getTransactionRepository(),
                validationConfiguration);
    }

    public BankTransactionService getTransactionService() {
        return transactionService;
    }
}
