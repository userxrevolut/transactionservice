package org.nk.revolut.services.transaction.configuration;

import java.math.BigDecimal;

public class ValidationConfiguration {
    private String supportedCurrency;
    private BigDecimal maximumAccountBalace;
    private int maximumScale;

    public String getSupportedCurrency() {
        return supportedCurrency;
    }

    public void setSupportedCurrency(String supportedCurrency) {
        this.supportedCurrency = supportedCurrency;
    }

    public BigDecimal getMaximumAccountBalace() {
        return maximumAccountBalace;
    }

    public void setMaximumAccountBalace(BigDecimal maximumAccountBalace) {
        this.maximumAccountBalace = maximumAccountBalace;
    }

    public int getMaximumScale() {
        return maximumScale;
    }

    public void setMaximumScale(int maximumScale) {
        this.maximumScale = maximumScale;
    }
}
