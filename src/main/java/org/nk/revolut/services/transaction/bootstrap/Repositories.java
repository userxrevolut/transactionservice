package org.nk.revolut.services.transaction.bootstrap;

import org.nk.revolut.services.transaction.configuration.ServiceConfiguration;
import org.nk.revolut.services.transaction.repositories.*;
import org.nk.revolut.services.transaction.util.DBTransactionUtil;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class Repositories {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final DBTransactionUtil transactionUtil;

    public Repositories(ServiceConfiguration configuration) {
        EntityManagerFactory entityManagerFactory = createEntityManagerFactory(configuration.getDatabaseConnection());
        ThreadLocalEntityManagerProvider entityManagerProvider = new ThreadLocalEntityManagerProvider(entityManagerFactory);

        accountRepository = new JPAAccountRepository(entityManagerProvider);
        transactionRepository = new JPATransactionRepository(entityManagerProvider);
        transactionUtil = new DBTransactionUtil(entityManagerProvider);
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    public DBTransactionUtil getTransactionUtil() {
        return transactionUtil;
    }

    private EntityManagerFactory createEntityManagerFactory(String connectionUrl) {
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.connection.url", connectionUrl);
        return Persistence.createEntityManagerFactory("bankTransactionPersistanceUnit", properties);
    }
}
