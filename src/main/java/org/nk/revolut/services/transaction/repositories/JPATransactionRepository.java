package org.nk.revolut.services.transaction.repositories;

import org.nk.revolut.services.transaction.domain.Transaction;

import javax.persistence.EntityManager;
import java.util.function.Supplier;

public class JPATransactionRepository implements TransactionRepository {

    private final Supplier<EntityManager> entityManager;

    public JPATransactionRepository(Supplier<EntityManager> entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Transaction saveNew(Transaction transaction) {
        if (transaction.getId() != null) {
            throw new IllegalStateException("Only new transactions can be saved");
        }
        EntityManager em = entityManager.get();
        em.persist(transaction);
        em.flush();
        return transaction;
    }
}
