package org.nk.revolut.services.transaction.repositories;

import org.nk.revolut.services.transaction.domain.Transaction;

public interface TransactionRepository {

    Transaction saveNew(Transaction transaction);
}
