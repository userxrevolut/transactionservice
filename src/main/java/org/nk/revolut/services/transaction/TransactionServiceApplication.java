package org.nk.revolut.services.transaction;

import org.nk.revolut.services.transaction.bootstrap.HttpServer;
import org.nk.revolut.services.transaction.bootstrap.Repositories;
import org.nk.revolut.services.transaction.bootstrap.Services;
import org.nk.revolut.services.transaction.configuration.ServiceConfiguration;
import org.nk.revolut.services.transaction.configuration.ValidationConfiguration;

public class TransactionServiceApplication {

    private ServiceConfiguration configuration;
    private HttpServer server;
    private Repositories repositories;
    private Services services;

    public TransactionServiceApplication(ServiceConfiguration serviceConfiguration,
                                         ValidationConfiguration validationConfiguration,
                                         int port) throws Exception {
        this.configuration = serviceConfiguration;
        this.repositories = new Repositories(configuration);
        this.services = new Services(repositories, validationConfiguration);
        SharedContext.initialize(repositories, services);

        this.server = new HttpServer(port);
    }

    public void run() throws Exception {
        server.start();
    }
}
