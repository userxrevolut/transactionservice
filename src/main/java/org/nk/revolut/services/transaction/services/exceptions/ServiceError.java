package org.nk.revolut.services.transaction.services.exceptions;

import org.nk.revolut.services.transaction.services.ErrorCodes;

public class ServiceError extends Exception {
    private final ErrorCodes code;

    public ServiceError(ErrorCodes code) {
        super(code.name());
        this.code = code;
    }

    public ErrorCodes getCode() {
        return code;
    }
}
