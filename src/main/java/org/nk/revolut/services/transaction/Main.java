package org.nk.revolut.services.transaction;

import org.nk.revolut.services.transaction.configuration.ServiceConfiguration;
import org.nk.revolut.services.transaction.configuration.ServiceConfigurationLoader;
import org.nk.revolut.services.transaction.configuration.ValidationConfiguration;

public class Main {

    public static final String DEFAULT_SERVICE_CONFIGURATION_PATH = "/configuration/transactional-service-configuration.json";
    public static final String DEFAULT_SERVICE_VALIDATION_CONFIGURATION_PATH = "/configuration/transactional-service-validation.json";

    public static void main(String[] args) throws Exception {

        Integer port = null;
        if (args.length > 0) {
            if ("--help".equals(args[0])) {
                System.out.println("exec port");
                System.out.println("For instance");
                System.out.println("exec http://localhost:8080");
            } else {
                port = Integer.parseInt(args[0]);
            }
        }
        ServiceConfigurationLoader configurationLoader = new ServiceConfigurationLoader();
        ServiceConfiguration configuration = configurationLoader.loadFromClassPath(DEFAULT_SERVICE_CONFIGURATION_PATH);

        if (port == null) {
            port = configuration.getDefaultServerPort();
        }

        ValidationConfiguration validationConfiguration = configurationLoader.loadValidationConfigurationFromClassPath(DEFAULT_SERVICE_VALIDATION_CONFIGURATION_PATH);

        TransactionServiceApplication application = new TransactionServiceApplication(configuration, validationConfiguration, port);

        application.run();
    }
}
