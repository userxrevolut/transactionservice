package org.nk.revolut.services.transaction.services.exceptions;

import org.nk.revolut.services.transaction.services.ErrorCodes;

public class ValidationException extends ServiceError {

    private final String field;
    private final Object value;

    public ValidationException(ErrorCodes code, String field, Object value) {
        super(code);
        this.field = field;
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public Object getValue() {
        return value;
    }
}
