package org.nk.revolut.services.transaction.services;

public enum ErrorCodes {
    AccountNotFould,
    InsufficientFunds,
    OtherError,
    UnsupportedCurrency,
    WrongAmount
}
