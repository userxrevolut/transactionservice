package org.nk.revolut.services.transaction.services;

import org.nk.revolut.services.transaction.configuration.ValidationConfiguration;
import org.nk.revolut.services.transaction.domain.Account;
import org.nk.revolut.services.transaction.domain.Transaction;
import org.nk.revolut.services.transaction.dto.TransactionRequest;
import org.nk.revolut.services.transaction.dto.TransactionResult;
import org.nk.revolut.services.transaction.repositories.AccountRepository;
import org.nk.revolut.services.transaction.repositories.TransactionRepository;
import org.nk.revolut.services.transaction.services.exceptions.ValidationException;
import org.nk.revolut.services.transaction.services.validation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class SingleCurrencyTransactionService implements BankTransactionService {

    private static Logger logger = Logger.getLogger(SingleCurrencyTransactionService.class.getCanonicalName());

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final List<TransactionRequestValidator> validators;
    private final PostTransactionAccountValidator postTransactionAccountValidator;

    public SingleCurrencyTransactionService(AccountRepository accountRepository, TransactionRepository transactionRepository, ValidationConfiguration validationConfiguration) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        this.validators = setupValidators(validationConfiguration);
        this.postTransactionAccountValidator = new PostTransactionAccountValidator(validationConfiguration.getMaximumAccountBalace());
    }

    private List<TransactionRequestValidator> setupValidators(ValidationConfiguration validationConfiguration) {
        return Arrays.asList(
                new AccountInformationValidator(),
                new CurrencyValidator(validationConfiguration.getSupportedCurrency()),
                (TransactionRequestValidator) new AmountValidator(validationConfiguration.getMaximumScale()));
    }

    @Override
    public TransactionResult performTransaction(TransactionRequest request) {
        try {
            validate(request);
        } catch (ValidationException ex) {
            logger.info("Transaction request validation failed " + ex.getCode().name()
                    + " for field " + ex.getField() + " and value " + ex.getValue());
            return TransactionResult.createFailedResult(ex.getCode().name());
        }

        Transaction transaction = null;
        try {
            transaction = doTransfer(
                    request.getSourceAccount().getAccountNumber(),
                    request.getTargetAccount().getAccountNumber(),
                    request.getTransferedAmount().getAmount());
        } catch (ValidationException ex) {
            logger.info("Transaction request action failed " + ex.getCode().name()
                    + " for field " + ex.getField() + " and value " + ex.getValue());
            return TransactionResult.createFailedResult(ex.getCode().name());
        }
        return TransactionResult.createSuccessfulResult(transaction.getId());
    }

    private Transaction doTransfer(String sourceAccountId, String targetAccountId, BigDecimal amount) throws ValidationException {
        Account sourceAccount = accountRepository.findByAccoundNumber(sourceAccountId);
        if (sourceAccount == null) {
            throw new ValidationException(ErrorCodes.AccountNotFould, "sourceAccount", sourceAccount);
        }
        Account targetAccount = accountRepository.findByAccoundNumber(targetAccountId);
        if (targetAccount == null) {
            throw new ValidationException(ErrorCodes.AccountNotFould, "targetAccount", sourceAccount);
        }
        return doTransfer(sourceAccount, targetAccount, amount);
    }

    private Transaction doTransfer(Account sourceAccount, Account targetAccount, BigDecimal amount) throws ValidationException {
        validateFunds(sourceAccount, amount);
        sourceAccount.setBalance(sourceAccount.getBalance().subtract(amount));
        targetAccount.setBalance(targetAccount.getBalance().add(amount));

        postTransactionAccountValidator.validate(sourceAccount);
        postTransactionAccountValidator.validate(targetAccount);

        sourceAccount = accountRepository.update(sourceAccount);
        targetAccount = accountRepository.update(targetAccount);

        return createTransaction(sourceAccount, targetAccount, amount);
    }

    private Transaction createTransaction(Account sourceAccount, Account targetAccount, BigDecimal amount) {
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setSourceAccount(sourceAccount);
        transaction.setTargetAccount(targetAccount);

        return transactionRepository.saveNew(transaction);
    }

    private void validateFunds(Account sourceAccount, BigDecimal amount) throws ValidationException {
        if (sourceAccount.getBalance().compareTo(amount) < 0) {
            throw new ValidationException(ErrorCodes.InsufficientFunds, "amount", amount);
        }
    }

    private void validate(TransactionRequest request) throws ValidationException {
        for (TransactionRequestValidator validator : validators) {
            validator.validate(request);
        }
    }
}
